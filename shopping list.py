
shopList = []

add= input("Do you want to add something to your list? Y or N?")

while add.lower() == "y":
    item = input("Type in your desired item to the list:")
    shopList.append(item)
    add = input("Want to add more to your list? Y or N?")

print()
print("Here is your shopping list.")
shopList.sort()
for listItem in shopList:
    print(listItem)
